#========================================================
# RPILEDA
# -------
#
# This program gets data over the CAN bus and controls an
# LED connected to port GPIO2. The LED is turned ON 3 secs
#
# File : RPILEDA.py
# Date : December 2022
#==========================================================

import RPi.GPIO as GPIO
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
import can # Import can
import time # Import time
LEDA = 2 # LEDA on port 2
GPIO.setup(LEDA, GPIO.OUT) # LED is output
GPIO.output(LEDA, 0) # LED OFF
bus=can.interface.Bus(bustype='socketcan',channel='can0',bitrate=250000)
#
# Set ID to 0x60 and filter to 0xFF
#
filters=[{"can_id":0x60, "can_mask": 0xFF, "extended":False}]
while True:
 dat = bus.recv() # Receive data
 if dat.data == bytearray(b'\x02'): # Data = 2?
 GPIO.output(LEDA, 1) # LEDA ON
 time.sleep(3) # 3 secs
 GPIO.output(LEDA, 0) # LEDA OFF
/*--------------------------------------------------------
 ARDLEDB
 =======
This program receives messages over the CAN bus and
controls the LED connected to port pin 2
File : ARDLEDB
Date : December 2022
---------------------------------------------------------*/
#include <SPI.h>
#include <mcp2515.h>
#define CS 10
int LED = 2;
char resp;
struct can_frame MyMsg;
MCP2515 mcp2515(CS); // SPI CS
void setup()
{
 mcp2515.reset();
 mcp2515.setBitrate(CAN_250KBPS, MCP_8MHZ); // CAN bus config
 mcp2515.setNormalMode(); // Normal mode
 pinMode(LED, OUTPUT);
 digitalWrite(LED, 0);
}
void loop()
{
 if(mcp2515.readMessage(&MyMsg) == MCP2515::ERROR_OK)
 {
 resp = MyMsg.data[0];
 if(MyMsg.can_id == 0x50 && resp == 1)
 {
 digitalWrite(LED, 1);
 delay(3000);
 digitalWrite(LED, 0);
 }
 }
}
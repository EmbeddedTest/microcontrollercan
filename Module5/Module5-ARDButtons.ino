/*-------------------------------------------------------------
 ARDBUTTONS
 ==========
Two buttons are connected to this node. Pressing button BUTTO
sends a request over the CAN bus to turn ON LEDA. Similarly,
pressing BUTTONB sends a request to turn ON LEDB
File : ARDBUTTONS
Date : December 2022
----------------------------------------------------------------*/
#include <SPI.h>
#include <mcp2515.h>
#define CS 10
int ReceivedMessage;
int BUTTONA = 2; // R button
int BUTTONB = 3; // G button
int T, ID, flag = 0;
struct can_frame MyMsg;
MCP2515 mcp2515(CS);
void setup()
{
 SPI.begin();
 mcp2515.reset();
 mcp2515.setBitrate(CAN_250KBPS, MCP_8MHZ); // Config CAN bus
 mcp2515.setNormalMode(); // Normal mode
 pinMode(BUTTONA, INPUT_PULLUP); // BUTTONA is input
 pinMode(BUTTONB, INPUT_PULLUP); // BUTTONB is input
}
void loop()
{
 if(digitalRead(BUTTONA) == 0) // BUTTONA pressed
 {
 while(digitalRead(BUTTONA) == 0);
 MyMsg.data[0] = 1;
 MyMsg.can_id = 0x50; // ID = 0x50
 flag = 1;
 }

 if(digitalRead(BUTTONB) == 0) // BUTTONB pressed
 {
 while(digitalRead(BUTTONB) == 0);
 MyMsg.data[0] = 2;
 MyMsg.can_id = 0x60; // ID = 0x60
 flag = 1;
 }
 if(flag == 1) // If any button pressed
 {
 flag = 0;
 MyMsg.can_dlc = 1; // 1 bytes
 mcp2515.sendMessage(&MyMsg); // Send message
 }
}
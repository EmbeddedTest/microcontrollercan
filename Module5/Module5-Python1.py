#========================================================
# RPIBUTTONS
# ----------
#
# This program reads the state of two buttons named BUTTONA
# and BUTTONB. Pressing BUTTONA sends character A over the
# cab bus. Similarly, pressing BUTTONB sends character B over
# the CAN bus
#
# File : RPIBUTTONS.py
# Date : December 2022
#==========================================================
import RPi.GPIO as GPIO
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
import can # Import can
import time # Import time
BUTTONA = 2 # BUTTONA on port 2
BUTTONB = 3 # BUTTONB on port 3
GPIO.setup(BUTTONA, GPIO.IN) # Input
GPIO.setup(BUTTONB, GPIO.IN) # Input
bus=can.interface.Bus(bustype='socketcan',channel='can0',bitrate=250000)
flag = 0
while True:
 if GPIO.input(BUTTONA) == 0: # BUTTONA pressed?
 while GPIO.input(BUTTONA) == 0: # BUTTONA released?
 pass
 stat = bytearray(b'A') # To send A
 flag = 1
 if GPIO.input(BUTTONB) == 0: # BUTTONB pressed?
 while GPIO.input(BUTTONB) == 0: # BUTTONB released
 pass
 stat = bytearray(b'B') # To send B
 flag = 1
#
# If a button is pressed then flag=1 and send msg to CAN bus
#
 if flag == 1:
 flag = 0
 msg = can.Message(arbitration_id=100, data=stat, is_extended_id=False)
 bus.send(msg)
 time.sleep(1)

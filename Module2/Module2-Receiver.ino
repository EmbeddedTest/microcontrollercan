	/*--------------------------------------------------------
	RECEIVER
	========
	In this program an LED is connected to port pin 2. The LED
	is turned ON for 5 seconds when the button at node SENDER
	is pressed
	
	File : RECEIVER
	Date : December 2022
	---------------------------------------------------------*/
	#include <SPI.h>
	#include <mcp2515.h>
	#define CS 10
	int ReceivedMessage;
	int LED = 2; // LED at port 2
	struct can_frame MyMsg;
	MCP2515 mcp2515(CS);
	void setup()
{
	SPI.begin();
	mcp2515.reset();
	mcp2515.setBitrate(CAN_500KBPS, MCP_8MHZ); // Config CAN bus
	mcp2515.setNormalMode(); // Normal mode
	pinMode(LED, OUTPUT); // LED is output
	digitalWrite(LED, 0); // LED OFF
}
	void loop()
{
	if(mcp2515.readMessage(&MyMsg) == MCP2515::ERROR_OK)
	{
	ReceivedMessage = MyMsg.data[0]; // Received data
	if(ReceivedMessage == 1) // If 1 is received
	{
	digitalWrite(LED, 1); // Turn ON LED
	delay(5000); // Wait 5 seconds
	digitalWrite(LED, 0); // Turn OFF LED
	}
	else
	digitalWrite(LED, 0);
	}
}
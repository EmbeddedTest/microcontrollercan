/*--------------------------------------------------------
 RECEIVERINT
 ===========
This program receives command from node SENDER. The program
is interrupt driven. When a message is received over the CAN
bus, an external interrupt is generated where the command is
read.If the comamnd is 1 then the LED is turned ON for 5 seconds
File : RECEIVERINT
Date : December 2022
---------------------------------------------------------*/
#include <SPI.h>
#include <mcp2515.h>
#define CS 10
int LED = 2;
int InterruptPin = 3;
int irq;
volatile int flag = 0;
struct can_frame MyMsg;
MCP2515 mcp2515(CS);
//
// Interrupt service routine
//
void MyISR()
{
 irq = mcp2515.getInterrupts(); // Get type of interrupt
 if(irq & MCP2515::CANINTF_RX0IF) // RX0IF ?
 {
 if(mcp2515.readMessage(MCP2515::RXB0, &MyMsg) == MCP2515::ERROR_OK)
 {
 mcp2515.clearInterrupts(); // Clear MCP2515 interrupts
 flag = MyMsg.data[0]; // Assign to flag
 }
 }
}
//
// CAN interfce module configuration and interrupt config on pin 3
//
void setup()
{
 SPI.begin();
 mcp2515.reset();
 mcp2515.setBitrate(CAN_500KBPS, MCP_8MHZ); // CAN bus configure
 mcp2515.setNormalMode(); // Normal mode
 pinMode(LED, OUTPUT); // LED is output
 digitalWrite(LED, 0); // LED is OFF
 pinMode(InterruptPin, INPUT_PULLUP);
 attachInterrupt(digitalPinToInterrupt(InterruptPin), MyISR, FALLING);
}
void loop()
{
 if(flag == 1) // If interrupt and 1 received
 {
 flag = 0;
 digitalWrite(LED, 1); // LED ON
 delay(5000); // 5 seconds delay
 digitalWrite(LED, 0); // LED OFF
 }
}
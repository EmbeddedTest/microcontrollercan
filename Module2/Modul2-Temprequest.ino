/*--------------------------------------------------------
 TEMPREQ
 =======
This program sends requests to node TEMPSENSE to read the
ambient temperature. The received temperature is displayed
on the Serial Monitor. A request is the characters T?.
Requests are made at every 5 seconds
File : TEMPREQ
Date : December 2022
---------------------------------------------------------*/
#include <SPI.h>
#include <mcp2515.h>
#define CS 10
int ReceivedMessage;
struct can_frame MyMsg;
MCP2515 mcp2515(CS);
void setup()
{
 SPI.begin();
 mcp2515.reset();
 mcp2515.setBitrate(CAN_500KBPS, MCP_8MHZ); // Config CAN bus
 mcp2515.setNormalMode(); // Normal mode
 Serial.begin(9600); // Serial Monitor
}
void loop()
{
 MyMsg.can_id = 0x30; // CAN ID
 MyMsg.can_dlc = 2; // 2 bytes
 MyMsg.data[0] = 'T'; // Load T
 MyMsg.data[1] = '?'; // Load ?
 mcp2515.sendMessage(&MyMsg); // Send message

 if(mcp2515.readMessage(&MyMsg) == MCP2515::ERROR_OK)
 {
 ReceivedMessage = MyMsg.data[0]; // REceived data
 Serial.print("Temperature = ");
 Serial.println(ReceivedMessage);
 Serial.println("");
 }
 delay(5000); // Wait 5 secs
}
F
/*--------------------------------------------------------
 SENDER
 ======
In this program a button is connecte to pin 2 of node
SENDER. Pressing the button sends logic 1 to node RECEIVER
where the LED at node RECEIVER is turned on for 5 seconds.
The button is pulled-up in software
Author: Dogan Ibrahim
File : SENDER
Date : December 2022
---------------------------------------------------------*/
#include <SPI.h>
#include <mcp2515.h>
#define CS 10
int Button= 2;
struct can_frame MyMsg;
MCP2515 mcp2515(CS);
void setup()
{
 SPI.begin();
 mcp2515.reset();
 mcp2515.setBitrate(CAN_500KBPS, MCP_8MHZ);
 mcp2515.setNormalMode();
 pinMode(Button, INPUT_PULLUP); // Pullup button
}
void loop()
{
 while(digitalRead(Button) == 1); // Wait if not pressed
 while(digitalRead(Button) == 0); // Wait until released
 MyMsg.can_id = 0x30;
 MyMsg.can_dlc = 8;
 MyMsg.data[0] = 1; // Load 1
 MyMsg.data[1] = 0x00;
 MyMsg.data[2] = 0x00;
 MyMsg.data[3] = 0x00;
 MyMsg.data[4] = 0x00;
 MyMsg.data[5] = 0x00;
 MyMsg.data[6] = 0x00;
 MyMsg.data[7] = 0x00;
 mcp2515.sendMessage(&MyMsg); // Send message
 delay(10);
}
/*--------------------------------------------------------
 SENSOR
 ======
This program reads the ambient temperature and sends it to
node ALARM. If the temperature is above a preset value then
node ALARM activates a buzzer
File : SENSOR
Date : December 2022
---------------------------------------------------------*/
#include <SPI.h>
#include <mcp2515.h>
#define CS 10
int T = A0;
int val = 0;
float temp;
struct can_frame MyMsg;
MCP2515 mcp2515(CS); // SPI CS
void setup()
{
 SPI.begin();
 mcp2515.reset();
 mcp2515.setBitrate(CAN_500KBPS, MCP_8MHZ); // CAN bus config
 mcp2515.setNormalMode(); // Normal mode
}
void loop()
{
 val = analogRead(T);
 temp = val * 5000.0 / 1024.0; // Convert to mV
 temp = temp / 10.0; // COnvert to degrees C
 MyMsg.can_id = 0x30; // CAN ID
 MyMsg.can_dlc = 8; // 8 bytes
 MyMsg.data[0] = (int)temp; // Temp in data[0]
 MyMsg.data[1] = 0x00; // Remainders 0
 MyMsg.data[2] = 0x00;
 MyMsg.data[3] = 0x00;
 MyMsg.data[4] = 0x00;
  MyMsg.data[5] = 0x00;
 MyMsg.data[6] = 0x00;
 MyMsg.data[7] = 0x00;
 mcp2515.sendMessage(&MyMsg); // Send message
 delay(3000); // Wait 3 secs
}
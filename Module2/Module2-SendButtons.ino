/*-------------------------------------------------------------
 BUTTONS
 =======
Three buttons are connected to this node. Pressing a button
turns ON the appropriate LED colour on node RGBLED. For example,
pressing R button turns ON the RED LED. The following data is
sent over the CAN bus:
1 Turn ON RED LED
2 Turn ON GREEN LED
3 Turn ON BLUE LED
File : BUTTONS
Date : December 2022
----------------------------------------------------------------*/
#include <SPI.h>
#include <mcp2515.h>
#define CS 10
int ReceivedMessage;
int R = 2; // R button
int G = 3; // G button
int B = 4; // B button
int flag = 0;
struct can_frame MyMsg;
MCP2515 mcp2515(CS);
void setup()
{
 SPI.begin();
 mcp2515.reset();
 mcp2515.setBitrate(CAN_500KBPS, MCP_8MHZ); // Config CAN bus
 mcp2515.setNormalMode(); // Normal mode
 pinMode(R, INPUT_PULLUP); // R Button is input
 pinMode(G, INPUT_PULLUP); // G button is input
 pinMode(B, INPUT_PULLUP); // B button is input
}
void loop() 
{
 if(digitalRead(R) == 0) // R pressed
 {
 while(digitalRead(R) == 0);
 MyMsg.data[0] = 1;
 flag = 1;
 }

 if(digitalRead(G) == 0) // G pressed
 {
 while(digitalRead(G) == 0);
 MyMsg.data[0] = 2;
 flag = 1;
 }

 if(digitalRead(B) == 0) // B pressed
 {
 while(digitalRead(B) == 0);
 MyMsg.data[0] = 3;
 flag =1;
 }
 if(flag == 1) // If any button pressed
 {
 flag = 0;
 MyMsg.can_id = 0x30; // CAN ID
 MyMsg.can_dlc = 1; // 1 bytes
 mcp2515.sendMessage(&MyMsg); // Send message
 }

 delay(20);
}
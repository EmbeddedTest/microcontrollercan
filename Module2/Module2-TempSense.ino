/*--------------------------------------------------------
 TEMPSENSE
 =========
This program gets requests from node TEMPREQ and then sends
the ambient temperature to node TEMPREQ. A request is the
characters T?
File : TEMPSENSE
Date : December 2022
---------------------------------------------------------*/
#include <SPI.h>
#include <mcp2515.h>
#define CS 10
int T = A0;
int val = 0;
float temp;
char msg1, msg2;
struct can_frame MyMsg;
MCP2515 mcp2515(CS); // SPI CS
void setup()
{
 SPI.begin();
 mcp2515.reset();
 mcp2515.setBitrate(CAN_500KBPS, MCP_8MHZ); // CAN bus config
 mcp2515.setNormalMode(); // Normal mode
}
void loop()
{
 if(mcp2515.readMessage(&MyMsg) == MCP2515::ERROR_OK)
 {
 msg1 = MyMsg.data[0]; // Received data1
 msg2 = MyMsg.data[1]; // REceived data2

 if(msg1 == 'T' && msg2 == '?') // If T? received
 {
 val = analogRead(T);
 temp = val * 5000.0 / 1024.0; // Convert to mV
 temp = temp / 10.0; // COnvert to degrees C
 MyMsg.can_id = 0x30; // CAN ID
 MyMsg.can_dlc = 1; // 1 byte
 MyMsg.data[0] = (int)temp; // Temp in data[0]
 mcp2515.sendMessage(&MyMsg); // Send message
 }
 }
}
/*--------------------------------------------------------------
 RGBLED
 ======
A RGB LED is connected to this node. LED colours Red, Green, or
Blue are lit if 1, 2, or are received over the CAN bus respectively
File : RGBLED
Date : December 2022
---------------------------------------------------------------*/
#include <SPI.h>
#include <mcp2515.h>
#define CS 10
int LED;
int Red = 2; // Red LED
int Green = 3; // Green LED
int Blue = 4; // Blue LED
struct can_frame MyMsg;
MCP2515 mcp2515(CS);
void setup()
{
 SPI.begin();
 mcp2515.reset();
 mcp2515.setBitrate(CAN_500KBPS, MCP_8MHZ); // Config CAN bus
 mcp2515.setNormalMode(); // Normal mode
 pinMode(Red, OUTPUT); // LED is output
 pinMode(Green, OUTPUT); // LED is output
 pinMode(Blue, OUTPUT); // LED is output
 digitalWrite(Red, 0); // Red OFF OFF
 digitalWrite(Green, 0); // Green OFF
 digitalWrite(Blue, 0); // Blue OFF
}
void loop()
{
 if(mcp2515.readMessage(&MyMsg) == MCP2515::ERROR_OK)
 {
 LED = MyMsg.data[0]; // Received data
 if(LED == 1)
 {
 digitalWrite(Red, 1);
 delay(2000);
  digitalWrite(Red, 0);
 }
 if(LED == 2)
 {
 digitalWrite(Green, 1);
 delay(2000);
 digitalWrite(Green, 0);
 }
 if(LED == 3)
 {
 digitalWrite(Blue, 1);
 delay(2000);
 digitalWrite(Blue, 0);
 }
 }
}
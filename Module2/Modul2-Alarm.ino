/*--------------------------------------------------------
 ALARM
 =====
This program received the temperature from node SENSOR.If
thetemperature is above a preset value then a buzzer is
activated. The preset value is set to 22 Degrees C here
File : ALARM
Date : December 2022
---------------------------------------------------------*/
#include <SPI.h>
#include <mcp2515.h>
#define CS 10
int ReceivedMessage;
int Buzzer = 2; // Buzzer at port 2
int PresetTemp = 22; // Preset temperature
struct can_frame MyMsg;
MCP2515 mcp2515(CS);
void setup()
{
 SPI.begin();
 mcp2515.reset();
 mcp2515.setBitrate(CAN_500KBPS, MCP_8MHZ); // Config CAN bus
 mcp2515.setNormalMode(); // Normal mode
 pinMode(Buzzer, OUTPUT); // Buzzer is output
 digitalWrite(Buzzer, 0); // Buzzer OFF
}

void loop()
{
 if(mcp2515.readMessage(&MyMsg) == MCP2515::ERROR_OK)
 {
 ReceivedMessage = MyMsg.data[0]; // REceived data
 if(ReceivedMessage > PresetTemp)
 digitalWrite(Buzzer, 1);
 else
 digitalWrite(Buzzer, 0);
 }
}
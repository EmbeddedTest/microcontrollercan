/*--------------------------------------------------------
 SND
 ===
This program generates random numbers between 1 and 100 and
sends them over the CAN bus. Node CATCH catches all the
data on the bus and tabulates the message ID, message size,
and the message itself.
File : SND
Date : December 2022
---------------------------------------------------------*/
#include <SPI.h>
#include <mcp2515.h>
#define CS 10
int RandomNumber;
struct can_frame MyMsg;
MCP2515 mcp2515(CS); // SPI CS
void setup()
{
 SPI.begin();
 mcp2515.reset();
 mcp2515.setBitrate(CAN_500KBPS, MCP_8MHZ); // CAN bus config
 mcp2515.setNormalMode(); // Normal mode
}
void loop()
{
 RandomNumber = random(1, 101); // Random number
 MyMsg.can_id = 0x30; // CAN ID
 MyMsg.can_dlc = 1; // 1 byte
 MyMsg.data[0] = RandomNumber; // Number in data[0]
 mcp2515.sendMessage(&MyMsg); // Send message
 delay(5000); // Wait 5 secs
}
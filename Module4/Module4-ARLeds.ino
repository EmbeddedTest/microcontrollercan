/*--------------------------------------------------------
 ARLEDS
 =====
This program receives messages over the CAN bus and
controls 2 LEDS at ports 2 and 3
File : ARLEDS
Date : December 2022
---------------------------------------------------------*/
#include <SPI.h>
#include <mcp2515.h>

#define CS 10
int LEDA = 2;
int LEDB = 3;
char resp;
struct can_frame MyMsg;
MCP2515 mcp2515(CS); // SPI CS
void setup()
{
 mcp2515.reset();
 mcp2515.setBitrate(CAN_250KBPS, MCP_8MHZ); // CAN bus config
 mcp2515.setNormalMode(); // Normal mode
 pinMode(LEDA, OUTPUT);
 pinMode(LEDB, OUTPUT);
 digitalWrite(LEDA, 0);
 digitalWrite(LEDB, 0);
}
void loop()
{
 if(mcp2515.readMessage(&MyMsg) == MCP2515::ERROR_OK)
 {
 resp = MyMsg.data[0];
 if(resp == 'A')
 {
 digitalWrite(LEDA, 1);
 delay(3000);
 digitalWrite(LEDA, 0);
 }

 if(resp == 'B')
 {
 digitalWrite(LEDB, 1);
 delay(3000);
 digitalWrite(LEDB, 0);
 }
 }
}

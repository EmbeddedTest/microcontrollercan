/*--------------------------------------------------------
 ARCATCH
 ========
This program receives data over the CAN bus and displays the
message on the Serial Monitor
File : ARCATCH
Date : December 2022
---------------------------------------------------------*/
#include <SPI.h>
#include <mcp2515.h>
#define CS 10
char msg;
struct can_frame MyMsg;
MCP2515 mcp2515(CS); // SPI CS
void setup()
{
 Serial.begin(9600); // Serial Monitor
 SPI.begin();
 mcp2515.reset();
 mcp2515.setBitrate(CAN_250KBPS, MCP_8MHZ); // CAN config
 mcp2515.setNormalMode(); // Normal mode
 Serial.println("Start");
}
void loop()
{
 if(mcp2515.readMessage(&MyMsg) == MCP2515::ERROR_OK)
 {
 Serial.println(«Received»);
 for(int j = 0; j < 8; j++)
 {
 msg = MyMsg.data[j];
 Serial.println(msg);
 }
 }
}
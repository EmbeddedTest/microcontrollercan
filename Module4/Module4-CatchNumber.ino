/*--------------------------------------------------------
 CATCH
 =====
This program receives data over the CAN bus and displays the
message ID, message size, and the contents of the message.
The data is displayed on the Serial Monitor
File : CATCH
Date : December 2022
---------------------------------------------------------*/
#include <SPI.h>
#include <mcp2515.h>
#define CS 10
int msg, id, sz;
struct can_frame MyMsg;
MCP2515 mcp2515(CS); // SPI CS
void setup()
{
 Serial.begin(9600); // Serial Monitor
 Serial.println("ID\tSize\tMessage");
 Serial.println("==\t====\t=======\t");

 SPI.begin();
 mcp2515.reset();
 mcp2515.setBitrate(CAN_500KBPS, MCP_8MHZ); // CAN config
 mcp2515.setNormalMode(); // Normal mode
}
void loop()
{
 if(mcp2515.readMessage(&MyMsg) == MCP2515::ERROR_OK)
 {
 msg = MyMsg.data[0];
 id = MyMsg.can_id;
 sz = MyMsg.can_dlc;
 Serial.print(id);
 Serial.print("\t");
  Serial.print(sz);
 Serial.print("\t");
 Serial.println(msg);
 }
}
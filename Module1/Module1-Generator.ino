/*--------------------------------------------------------
 GENERATOR
 =========
This program generates random numbers between 1 and 20 and
sends them to the SQUARER program over the CAN bus. The
SQUARER program takes the square of the received numbers
and then displays them on the Serial Monitor
Author: Dogan Ibrahim
File : GENERATOR
Date : December 2022
---------------------------------------------------------*/
#include <SPI.h>
#include <mcp2515.h>
#define CS 10
int RandomNumber;
struct can_frame MyMsg;
MCP2515 mcp2515(CS); // SPI CS
void setup()
{
 SPI.begin();
 mcp2515.reset();
 mcp2515.setBitrate(CAN_500KBPS, MCP_8MHZ); // CAN bus config
 mcp2515.setNormalMode(); // Normal mode
}
void loop()
{
 RandomNumber = random(1, 21); // Random number
 MyMsg.can_id = 0x30; // CAN ID
 MyMsg.can_dlc = 8; // 8 bytes
 MyMsg.data[0] = RandomNumber; // Number in data[0]

 MyMsg.data[1] = 0x00; // Remainders 0
 MyMsg.data[2] = 0x00;
 MyMsg.data[3] = 0x00;
 MyMsg.data[4] = 0x00;
 MyMsg.data[5] = 0x00;
 MyMsg.data[6] = 0x00;
 MyMsg.data[7] = 0x00;
 mcp2515.sendMessage(&MyMsg); // Send message
 delay(5000); // Wait 5 secs
}
/*--------------------------------------------------------
 SQUARER
 =======
This program receives random numbers between 1 and 20 and
takes the square of these numbers and then displays the
numbers on the Serial Monitor
Author: Dogan Ibrahim
File : Squarer
Date : December 2022
---------------------------------------------------------*/
#include <SPI.h>
#include <mcp2515.h>
#define CS 10
int ReceivedNumber;
struct can_frame MyMsg;
MCP2515 mcp2515(CS); // SPI CS

void setup()
{
 Serial.begin(9600); // Serial Monitor
 Serial.println("CAN BUS");
 SPI.begin();
 mcp2515.reset();
 mcp2515.setBitrate(CAN_500KBPS, MCP_8MHZ); // CAN config
 mcp2515.setNormalMode(); // Normal mode
}
void loop()
{
 if(mcp2515.readMessage(&MyMsg) == MCP2515::ERROR_OK)
 {
 ReceivedNumber = MyMsg.data[0];
 Serial.print("Received Number is: ");
 Serial.println(ReceivedNumber);
 Serial.print("Square is: ");
 Serial.println(ReceivedNumber * ReceivedNumber);
 Serial.println("");
 }
}

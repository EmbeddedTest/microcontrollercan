/*--------------------------------------------------------
 THDISP2
 =======
This program receives the temperature and humidity from node
THSENSOR and displays on I2C LCD.
In this version of the program the temperature is displayed
File : THDISP2
Date : December 2022
---------------------------------------------------------*/
#include <SPI.h>
#include <mcp2515.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x27, 16, 2);
#define CS 10
int H;
char T[7];
struct can_frame MyMsg;
MCP2515 mcp2515(CS);
void setup()
{
 SPI.begin();
 mcp2515.reset();
 mcp2515.setBitrate(CAN_500KBPS, MCP_8MHZ); // Config CAN bus
 mcp2515.setNormalMode(); // Normal mode
 lcd.init(); // Initialize LCD
 lcd.backlight(); // Backlight ON
}
void loop()
{
 if(mcp2515.readMessage(&MyMsg) == MCP2515::ERROR_OK)
 {
 for(int i=0; i < 5; i++)T[i] = MyMsg.data[i];
 H = MyMsg.data[5]; // Humidity
 lcd.clear(); // Clear LCD
 lcd.setCursor(0, 0); // Cursor at 0,0
 lcd.print(T[0]); // Display T
 lcd.print(T[1]);
 lcd.print(T[2]);
 lcd.print(T[3]);
 lcd.print(T[4]);
 lcd.print(" C"); // DIsplay C
 lcd.setCursor(0, 1); // Cursor at 0,1
 lcd.print(H); // Display H

 lcd.print(" %"); // Display %
 }
}
/*--------------------------------------------------------
 DISPTEMP2
 =========
This program receives the external and internal temperature
readings over the CAN bus and displays them on the LCD. The
external temperature is displayed on the top row while the
internal temperature is displayed on the bottom row.
In this version of the program acceptance mask and acceptance
filter are used
File : DISPTEMP2
Date : December 2022
---------------------------------------------------------*/
#include <SPI.h>
#include <mcp2515.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x27, 16, 2);
#define CS 10
int T, id;
struct can_frame MyMsg;
MCP2515 mcp2515(CS);
void setup()
{
 SPI.begin();
 mcp2515.reset();
 mcp2515.setBitrate(CAN_500KBPS, MCP_8MHZ); // Config CAN bus
 lcd.init(); // Initialize LCD
 lcd.backlight(); // Backlight ON

 mcp2515.setConfigMode();
 mcp2515.setFilterMask(MCP2515::MASK0, false, 0x7FE);
 mcp2515.setFilter(MCP2515::RXF0, false, 0x30);
 mcp2515.setNormalMode(); // Normal mode
}
void loop()
{
 if(mcp2515.readMessage(&MyMsg) == MCP2515::ERROR_OK)
  {
 T = MyMsg.data[0]; // Temperature
 id = MyMsg.can_id; // CAN ID
 if(id == 0x30) // If External
 {
 lcd.setCursor(0, 0); // Cursor at 0,0
 lcd.print("EXT: "); // Display EXT
 lcd.print(T); // Display T
 lcd.print(" C"); // Display C
 }

 if(id == 0x31) // If Internal
 {
 lcd.setCursor(0, 1); // Cursor at 0,1
 lcd.print("INT: "); // Display INT
 lcd.print(T); // Display T/
 lcd.print(" C"); // Display C
 }
 delay(20);
 }
}
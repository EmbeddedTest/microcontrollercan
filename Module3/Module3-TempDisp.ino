/*--------------------------------------------------------
 TEMPX
 =====
This program reads the external ambient temperature when a
request is received over the CAN bus. The reading is sent
with message of 1 and ID = 0x30
File : TEMPX
Date : December 2022
---------------------------------------------------------*/
#include <SPI.h>
#include <mcp2515.h>
#define CS 10
int T = A0;
int val = 0;
float temp;
int msg;
struct can_frame MyMsg;
MCP2515 mcp2515(CS); // SPI CS
void setup()
{
 SPI.begin();
 mcp2515.reset();
 mcp2515.setBitrate(CAN_500KBPS, MCP_8MHZ); // CAN bus config
 mcp2515.setNormalMode(); // Normal mode
}
void loop()
{
 if(mcp2515.readMessage(&MyMsg) == MCP2515::ERROR_OK)
 {
 msg = MyMsg.data[0]; // Temperature
 val = analogRead(T);
 temp = val * 5000.0 / 1024.0; // Convert to mV
 temp = temp / 10.0; // COnvert to degrees C

 if(msg == 1) // Request for external
 {
 MyMsg.can_id = 0x30; // CAN ID
 MyMsg.can_dlc = 1; // 1 byte
 MyMsg.data[0] = (int)temp; // Temp in data[0]
 mcp2515.sendMessage(&MyMsg); // Send message
 }
 }
}
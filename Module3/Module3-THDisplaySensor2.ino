/*--------------------------------------------------------
 THSENSOR2
 =========
This program reads the ambient temperature and humidity from
a DHT11 sensor and sends the data to node THDISP.
In this version of the program the temperature is converted
into an array of chars and sent over the CAN bus so that values
after the decimal point can also be displayed
File : THSENSOR2
Date : December 2022
---------------------------------------------------------*/
#include <SPI.h>
#include <mcp2515.h>
#include "DHT.h"
DHT dht;
#define CS 10
struct can_frame MyMsg;
MCP2515 mcp2515(CS); // SPI CS
int DHTPIN = 2; // DHT11 on pin 2
float temperature, humidity;
char ary[7];
void setup()
{
 SPI.begin();
 mcp2515.reset();
 mcp2515.setBitrate(CAN_500KBPS, MCP_8MHZ); // CAN bus config
 mcp2515.setNormalMode(); // Normal mode
 dht.setup(DHTPIN); // Setup DHT11
}
void loop()
{
 humidity = dht.getHumidity(); // GEt humidity
 temperature = dht.getTemperature(); // Get temperature
 MyMsg.can_id = 0x30; // CAN ID
 MyMsg.can_dlc = 6; // 6 bytes
 dtostrf(temperature, 5, 2, ary); // Convert to chars
 MyMsg.data[0] = ary[0]; // Temp in data[0]
 MyMsg.data[1] = ary[1];
 MyMsg.data[2] = ary[2];
 MyMsg.data[3] = ary[3];
 MyMsg.data[4] = ary[4];
 MyMsg.data[5] = (int)humidity; // Humidity
 mcp2515.sendMessage(&MyMsg); // Send message
 delay(5000); // Wait 5 seconds
}
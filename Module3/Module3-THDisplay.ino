/*--------------------------------------------------------
 THDISP
 ======
This program receives the temperature and humidity from node
THSENSOR and displays on I2C LCD
File : THDISP
Date : December 2022
---------------------------------------------------------*/
#include <SPI.h>
#include <mcp2515.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x27, 16, 2);
#define CS 10
int T, H;
struct can_frame MyMsg;
MCP2515 mcp2515(CS);
void setup()
{
 SPI.begin();
 mcp2515.reset();
 mcp2515.setBitrate(CAN_500KBPS, MCP_8MHZ); // Config CAN bus
 mcp2515.setNormalMode(); // Normal mode
 lcd.init(); // Initialize LCD
 lcd.backlight(); // Backlight ON
}
void loop()
{
 if(mcp2515.readMessage(&MyMsg) == MCP2515::ERROR_OK)
 {
 T = MyMsg.data[0]; // Temperature
 H = MyMsg.data[1]; // Humidity
 lcd.clear(); // Clear LCD
 lcd.setCursor(0, 0); // Cursor at 0,0
 lcd.print(T); // Display T
 lcd.print(" C"); // DIsplay C
 lcd.setCursor(0, 1); // Cursor at 0,1
 lcd.print(H); // Display H
 lcd.print(" %"); // Display %
 }
}

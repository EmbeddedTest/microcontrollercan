/*--------------------------------------------------------
 INTTEMP
 =======
This program reads the internal ambient temperature and sends
it over the CAN bus every 5 seconds
File : INTTEMP
Date : December 2022
---------------------------------------------------------*/
#include <SPI.h>
#include <mcp2515.h>
#define CS 10
int T = A0;
int val = 0;
float temp;
struct can_frame MyMsg;
MCP2515 mcp2515(CS); // SPI CS
void setup()
{
 SPI.begin();
 mcp2515.reset();
 mcp2515.setBitrate(CAN_500KBPS, MCP_8MHZ); // CAN bus config
 mcp2515.setNormalMode(); // Normal mode
}
void loop()
{
 val = analogRead(T);
 temp = val * 5000.0 / 1024.0; // Convert to mV
 temp = temp / 10.0; // COnvert to degrees C
 MyMsg.can_id = 0x31; // CAN ID
 MyMsg.can_dlc = 1; // 1 byte
 MyMsg.data[0] = (int)temp; // Temp in data[0]
 mcp2515.sendMessage(&MyMsg); // Send message
 delay(5000); // Wait 5 secs
}
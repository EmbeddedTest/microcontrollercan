/*--------------------------------------------------------
 THSENSOR
 ========
This program reads the ambient temperature and humidity from
a DHT11 sensor and sends the data to node THDISP
File : THSENSOR
Date : December 2022
---------------------------------------------------------*/
#include <SPI.h>
#include <mcp2515.h>
#include "DHT.h"
DHT dht;
#define CS 10
struct can_frame MyMsg;
MCP2515 mcp2515(CS); // SPI CS
int DHTPIN = 2; // DHT11 on pin 2
float temperature, humidity;
void setup()
{
 SPI.begin();
 mcp2515.reset();
 mcp2515.setBitrate(CAN_500KBPS, MCP_8MHZ); // CAN bus config
 mcp2515.setNormalMode(); // Normal mode
 dht.setup(DHTPIN); // Setup DHT11
}
void loop()
{
 humidity = dht.getHumidity(); // GEt humidity
 temperature = dht.getTemperature(); // Get temperature
 MyMsg.can_id = 0x30; // CAN ID
 MyMsg.can_dlc = 2; // 2 byte
 MyMsg.data[0] = (int)temperature; // Temp in data[0]
 MyMsg.data[1] = (int)humidity; // Humidity
 mcp2515.sendMessage(&MyMsg); // Send message
 delay(5000); // Wait 5 seconds
}